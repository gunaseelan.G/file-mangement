package com.example.FileManager.service;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.example.FileManager.entity.FileModel;
import com.example.FileManager.repository.FileRepository;

@Service
public class FileService {
	@Autowired
	private FileRepository fileRepository;

	public FileModel saveFile(MultipartFile file) throws IOException {
		FileModel fileModel=new FileModel();
		fileModel.setContentType(file.getContentType());
		fileModel.setFileName(file.getOriginalFilename());
		fileModel.setData(file.getBytes());
		
		return fileRepository.save(fileModel);
	}

}
